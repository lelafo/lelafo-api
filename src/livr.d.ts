declare module 'livr' {
  export interface LivrData {
    [key: string]: any
  }
  export interface LivrRules {
    [key: string]: any
  }

  export interface Errors {
    [key: string]: string
  }

  export interface AliasedRule {
    name: string
    rule: any
    error?: string
  }

  export class Validator {
    private errors: Errors
    static defaultAutoTrim: (val: boolean) => void
    constructor(livrRules: LivrRules, isAutoTrim?: boolean)

    public validate: (data: LivrData) => LivrData | false
    public registerAliasedRule: (alias: AliasedRule) => void
    public getErrors: () => Errors
  }
}
