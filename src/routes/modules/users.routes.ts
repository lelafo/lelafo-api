import {RouteConfig} from '../../lib/router'
import UserCreateService from '../../services/user/user.create.service'
// import amw from '../../middlewares/amw'

const users: RouteConfig[] = [
  {
    path: 'users/create',
    service: UserCreateService
  }
]
export default users