import * as Livr from 'livr'
import {LivrRules} from 'livr'
import X from '../lib/err'
import {E_PARAM} from '../config'

export default abstract class BaseService {
  abstract execute(cleanParams: any): Promise<any>
  abstract validationRules: LivrRules
  public async validate (params: any): Promise<any> {
    const validator = new Livr.Validator(this.validationRules)
    const cleanParams = validator.validate(params)
    if (cleanParams) {
      return cleanParams
    } else {
      const errors = validator.getErrors()
      throw new X(E_PARAM, errors)
    }
  }

  public async run (params: any): Promise<any> {
    const cleanParams = await this.validate(params)
    const result = await this.execute(cleanParams)
    return result
  }
}