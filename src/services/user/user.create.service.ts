import BaseService from '../base.service'
import User from '../../models/user.model'

export default class UserCreateService extends BaseService {
  validationRules = {
    name: 'required',
    email: ['required', 'email']
  }

  public async execute (cleanParams: any): Promise<any> {
    console.log('LOG: UserCreateService -> cleanParams', cleanParams)
    const result = await User.find()
    return result
  }
}