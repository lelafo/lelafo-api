import BaseService from '../base.service'

export default class UserCreateService extends BaseService {
  validationRules = {
    name: 'required',
    email: ['required', 'email']
  }

  public async execute (cleanParams: any): Promise<any> {
    console.log('LOG: UserCreateService -> cleanParams', cleanParams)
    // Берем модель User
    // вызываем метод .find с параметрами
    return 'Ответ от userService'
  }
}