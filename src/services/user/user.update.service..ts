import BaseService from '../base.service'

export default class UserCreateService extends BaseService {
  validationRules = {
    name: 'required',
    email: ['required', 'email']
  }

  public async execute (cleanParams: any): Promise<any> {
    console.log('LOG: UserCreateService -> cleanParams', cleanParams)
    // Берем модель юзер
    // вызываем статический метод update() - напишем в базой модели
    return 'Ответ от userService'
  }
}