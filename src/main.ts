const PFX = 'main'
import {JSRPC_HOST, JSRPC_PORT, MONGODB_URI} from './config'

import Http from './lib/http'
import {dispatcher} from './lib/jsrpc'

import Logger from './lib/logger'
import Router from './lib/router'
import routes from './routes/routes'
import Ctx from './lib/ctx'
import appinfo from './lib/appinfo'

import {connect} from './lib/mdb'

const L = new Logger(PFX)

async function main (): Promise<void> {
  L.info('connection to MongoDB...')
  await connect(MONGODB_URI)

  const R = new Router(routes)
  const Server = new Http({host: JSRPC_HOST, port: JSRPC_PORT})

  L.info('start server...')
  Server.start([
    (req, res, next): void => {
      req.ctx = new Ctx()
      next()
    },
    (req, res, next): void => {
      res.header('X-ServerName', `${appinfo.name} ${appinfo.version}`)
      next()
    },
    dispatcher(
      (route: string) => R.getRouteService(route),
      (route: string) => R.getRouteMws(route)
    )
  ])
}

main().catch(err => {
  L.error('main function', {err})
  process.exit(1)
})
