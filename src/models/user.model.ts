import BaseModel from './base.model'
import {coll} from '../lib/mdb'

// знаем название коллекции из конфига users
// выхываем коллекцию
export default class User extends BaseModel {

  // async create (): Promise<any> {}

  static async find (): Promise<any> {
    return await coll('users').find().toArray()
  }

  // async update (): Promise<any> {}
}