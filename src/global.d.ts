// eslint-disable-next-line @typescript-eslint/no-unused-vars
import Ctx from './lib/ctx'
declare global {
  namespace Express {
    interface Request {
      ctx: Ctx
    }
  }
}