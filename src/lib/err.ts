export const ERR_SYM = Symbol('ERR_SYM')
export default class Err {
  constructor (public code: string, public err: any) {}
}