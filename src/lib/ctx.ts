export default class Ctx {
  public props = {}

  public add (ctx: {[key: string]: any}): void {
    Object.assign(this.props, ctx)
  }
}