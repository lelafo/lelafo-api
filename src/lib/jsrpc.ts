const PFX = 'lib/jsrpc'
import {E_INTERNAL, E_METHOD, E_RPC} from '../config'
import { Request, Response } from 'express'
import Logger from './logger'
import X from './err'
import {Middleware} from './router'
import BaseService from '../services/base.service'

const L = new Logger(PFX)

export function dispatcher (
  getService: (method: string) => new () => BaseService,
  getMiddelwares: (method: string) => Middleware[] | undefined
): (req: Request, res: Response) => Promise<any> {
  return async (req, res): Promise<any> => {
    try {
      const path = req.body.method
      const params = req.body.params
      if (!path || !params) {
        throw new X(E_RPC, 'wrong request')
      }

      const Service = getService(path)
      console.log('LOG: Service', Service)
      if (!Service) {
        throw new X(E_METHOD, `unknown method ${Service}`)
      }

      const middlewares = getMiddelwares(path)
      if (middlewares) {
        for (const mw of middlewares) {
          await mw(req)
        }
      }
      const result = await new Service().run(params)
      res.json({result})
    } catch (err) {
      L.error('jsrpc dispather', err)
      if (err instanceof X) {
        res.json({err})
      } else {
        res.json({err: new X(E_INTERNAL, err)})
      }
    }
  }
}