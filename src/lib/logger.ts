import { inspect } from 'util'
import * as chalk from 'chalk'

function log (fn: (msg: string, data?: any) => void, msg: string, data?: any): void {
  if (data !== undefined) {
    fn(msg, inspect(data, {depth: 8}))
  } else {
    fn(msg)
  }
}

export default class Logger {
  constructor (private PFX: string) {}

  public debug (msg: string, data?: any): void {
    log(console.log, `${this.PFX} ${msg}`, data)
  }
  public info (msg: string, data?: any): void {
    log(console.info, `${this.PFX} ${chalk.green(msg)}`, data)
  }
  public warn (msg: string, data?: any): void {
    log(console.warn, `${this.PFX} ${chalk.yellow(msg)}`, data)
  }
  public error (msg: string, data?: any): void {
    log(console.error, `${this.PFX} ${chalk.red(msg)}`, data)
  }
}