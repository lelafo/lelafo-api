import { MongoClient, ObjectID, Db, Collection } from 'mongodb'

let globalClient: MongoClient
let globalDb: Db

export function newId (): ObjectID {
  return new ObjectID()
}

export function isOid (id: any): boolean {
  return ObjectID.isValid(id)
}

export function getColl (collName: string): Collection {
  return globalDb.collection(collName)
}

export async function connect (url: string): Promise<Db> {
  globalClient = await MongoClient.connect(url, {useNewUrlParser: true, useUnifiedTopology: true})
  globalDb = globalClient.db()
  return globalDb
}