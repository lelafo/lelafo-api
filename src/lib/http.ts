const PFX = 'lib/http'
import * as express from 'express'
import Logger from './logger'
import { RequestHandler } from 'express'

const L = new Logger(PFX)

export default class Http {
  constructor (
    private cfg: {host: string, port: number}
  ) {}

  public async start (middlewares: RequestHandler[]): Promise<void> {
    const app = express()
    app.disable('etag')
    app.disable('x-powered-by')
    app.use(express.json())

    for (const mw of middlewares) {
      app.use(mw)
    }

    app.on('error', err => {
      throw err
    })

    app.listen(this.cfg.port, this.cfg.host, () => {
      L.info(`App is running at http://${this.cfg.host}:${this.cfg.port}`)
    })
  }
}