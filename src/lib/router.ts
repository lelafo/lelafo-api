import {Dictionary} from './types'
import {Request} from 'express'
import BaseService from '../services/base.service'

export interface Middleware {
  (req: Request): Promise<void>
}

export interface RouteConfig {
  path: string,
  ctx?: any
  middlewares?: Middleware[]
  service: new () => BaseService
}

export default class Router {
  private map: Dictionary<RouteConfig> = {}
  constructor (routes: RouteConfig[]) {
    this.mapMaker(routes)
  }

  private mapMaker (routes: RouteConfig[]): void {
    for (const route of routes) {
      if (this.map[route.path]) {
        throw new Error(`dublicate path ${route.path}`)
      }
      this.map[route.path] = route
    }
  }

  public getRoute (path: string): RouteConfig {
    return this.map[path]
  }

  public getRouteService (path: string): new () => BaseService {
    const rout = this.getRoute(path)
    return rout && rout.service
  }

  public getRouteMws (path: string): Middleware[] | undefined {
    const rout = this.getRoute(path)
    return rout && rout.middlewares
  }
}