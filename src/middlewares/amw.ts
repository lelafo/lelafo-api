import {Request} from 'express'

export default async function (req: Request): Promise<void> {
  console.log('amw', req.ctx.props)
  const promise = new Promise((resolve) => {
    setTimeout(() => {
      resolve('result')
    }, 1000)
  })
  console.log('LOG: promise', await promise)
  req.ctx.add({test: 'test'})
}