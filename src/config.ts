// JSRPC
export const JSRPC_HOST = 'localhost'
export const JSRPC_PORT = 7048
export const MONGODB_URI = 'mongodb://127.0.0.1:27017'

// Err codes
export const E_INTERNAL = 'E_INTERNAL'
export const E_METHOD = 'E_METHOD'
export const E_PARAM = 'E_PARAM'
export const E_RPC = 'E_RPC'

// Coll names
export const COLL_USERS = 'users'